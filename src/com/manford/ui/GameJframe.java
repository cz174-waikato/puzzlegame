package com.manford.ui;

import javax.swing.*;

public class GameJframe extends JFrame {
    public GameJframe() {
        initJFrame();

        initJMenuBar();

        initImage();




        this.setVisible(true);
    }

    private void initImage() {
        int pictureNumber = 1;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                ImageIcon icon = new ImageIcon("D:\\idea\\IntelliJ IDEA 2023.1.4\\daima\\waikato\\project01puzzlegame\\image\\animal\\animal3\\"+pictureNumber+".jpg");
                JLabel jLabel = new JLabel(icon);
                jLabel.setBounds(105*j,105*i,105,105);
                // this.add(jLabel1);
                this.getContentPane().add(jLabel);
                pictureNumber++;
            }
        }

    }

    private void initJMenuBar() {
        //init menubar
        JMenuBar JMenuBar = new JMenuBar();
        JMenu functionJMenu = new JMenu("Function");
        JMenu aboutJMenu = new JMenu("About US");

        JMenuItem replyItem = new JMenuItem("Replay Game");
        JMenuItem reLoginItem = new JMenuItem("ReLogin");
        JMenuItem closeItem = new JMenuItem("Close Game");

        JMenuItem accountItem = new JMenuItem("Social Media");

        functionJMenu.add(replyItem);
        functionJMenu.add(reLoginItem);
        functionJMenu.add(closeItem);

        aboutJMenu.add(accountItem);

        JMenuBar.add(functionJMenu);
        JMenuBar.add(aboutJMenu);
        this.setJMenuBar(JMenuBar);
    }

    private void initJFrame() {
        this.setSize(603, 680);
        this.setTitle("puzzlegame v1.0");
        this.setAlwaysOnTop(true);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setLayout(null);//cancel default center
    }
}
