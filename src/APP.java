import com.manford.ui.GameJframe;
import com.manford.ui.LoginJFrame;
import com.manford.ui.RegisterJFrame;

public class APP {
    public static void main(String[] args) {
        new LoginJFrame();
        new RegisterJFrame();
        new GameJframe();
    }
}
